import requests
import os
from datetime import timedelta
from datetime import datetime
import time

#Get List domain in file
list_domain = []
with open("./domains.txt",'r',encoding='utf8') as f:
    file=f.read().splitlines()
    for line in file:
        list_domain.append(line)


today = datetime.now().date()
stamp1=time.mktime(today.timetuple())
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
#Function get expired_date
def get_expired_day(domain):
    r = requests.get("http://www.whois.net.vn/whois.php?domain=" + domain + "&act=getwhois",headers=headers)
    data = str(r.text)
    with open("data.txt",'w+', encoding='utf8') as f:
        f.write(data)
    with open("data.txt",'r',encoding='utf8') as f:
        file=f.read().splitlines()
        for line in file:
            if "Expired Date" in line:
                expired_day_str = line.split(" : ")[1].split("T")[0]
                expired_day_object = datetime.strptime(expired_day_str, '%Y-%m-%d').date()
                return expired_day_object

#Print result to file in AppManager format
with open("result.txt",'w+', encoding='utf8') as f:
    f.write("<--table Day_to_expire starts-->")
    f.write("\n")
    f.write("domain= expired_date= remaining_days= status")
    f.write("\n")
    for domain in list_domain:
        day_to_expire = get_expired_day(domain)
        f.write(domain)
        f.write(" = ")
        f.write(str(day_to_expire))
        f.write(" = ")
        stamp2=time.mktime(day_to_expire.timetuple())
        delta =stamp2 - stamp1
        remaining_days= int(delta/(60*60*24))
        f.write(str(remaining_days))
        if remaining_days <= 45:
            f.write(" = Warning")
        else:
            f.write(" = OK")
        f.write("\n")
    f.write("<--table Day_to_expire ends-->")

os.remove("data.txt")
print("Done")
